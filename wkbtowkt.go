// Copyright 2020 Roma Hicks

// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

package wellknowngeometry

import (
	"encoding/binary"
	"math"
	"strings"
)

// WKBToWKT takes a well-known binary array and converts it to a well-known text string
func WKBToWKT(wkb []byte) (string, int) {
	wkt := strings.Builder{}
	var byteCount int
	var geoString string

	endian := wkb[0]
	typeID := get32Number(wkb[1:5], endian)

	switch typeID {
	case 1:
		wkt.WriteString("POINT(")
		geoString, byteCount = wkbPoint(wkb)
	case 2:
		wkt.WriteString("LINESTRING(")
		geoString, byteCount = wkbLineString(wkb)
	case 3:
		wkt.WriteString("POLYGON(")
		geoString, byteCount = wkbPolygon(wkb)
	case 4:
		wkt.WriteString("MULTIPOINT(")
		geoString, byteCount = multi(wkbPoint, wkb)
	case 5:
		wkt.WriteString("MULTILINESTRING(")
		geoString, byteCount = multi(wkbLineString, wkb)
	case 6:
		wkt.WriteString("MULTIPOLYGON(")
		geoString, byteCount = multi(wkbPolygon, wkb)
	case 7:
		wkt.WriteString("GEOMETRYCOLLECTION(")
		geoString, byteCount = wkbGeometryCollection(wkb)
	case 15:
		wkt.WriteString("POLYHEDRALSURFACE(")
		geoString, byteCount = wkbPolyhedralSurfaceTIN(wkb)
	case 16:
		wkt.WriteString("TIN(")
		geoString, byteCount = wkbPolyhedralSurfaceTIN(wkb)
	case 1001:
		wkt.WriteString("POINT Z(")
		geoString, byteCount = wkbPoint_Z_M(wkb)
	case 1002:
		wkt.WriteString("LINESTRING Z(")
		geoString, byteCount = wkbLineString_Z_M(wkb)
	case 1003:
		wkt.WriteString("POLYGON Z(")
		geoString, byteCount = wkbPolygon_Z_M(wkb)
	case 1004:
		wkt.WriteString("MULTIPOINT Z(")
		geoString, byteCount = multi(wkbPoint_Z_M, wkb)
	case 1005:
		wkt.WriteString("MULTILINESTRING Z(")
		geoString, byteCount = multi(wkbLineString_Z_M, wkb)
	case 1006:
		wkt.WriteString("MULTIPOLYGON Z(")
		geoString, byteCount = multi(wkbPolygon_Z_M, wkb)
	case 1007:
		wkt.WriteString("GEOMETRYCOLLECTION Z(")
		geoString, byteCount = wkbGeometryCollection_Z_M(wkb)
	case 1015:
		wkt.WriteString("POLYHEDRALSURFACE Z(")
		geoString, byteCount = wkbPolyhedralSurfaceTIN_Z_M(wkb)
	case 1016:
		wkt.WriteString("TIN Z(")
		geoString, byteCount = wkbPolyhedralSurfaceTIN_Z_M(wkb)
	case 2001:
		wkt.WriteString("POINT M(")
		geoString, byteCount = wkbPoint_Z_M(wkb)
	case 2002:
		wkt.WriteString("LINESTRING M(")
		geoString, byteCount = wkbLineString_Z_M(wkb)
	case 2003:
		wkt.WriteString("POLYGON M(")
		geoString, byteCount = wkbPolygon_Z_M(wkb)
	case 2004:
		wkt.WriteString("MULTIPOINT M(")
		geoString, byteCount = multi(wkbPoint_Z_M, wkb)
	case 2005:
		wkt.WriteString("MULTILINESTRING M(")
		geoString, byteCount = multi(wkbLineString_Z_M, wkb)
	case 2006:
		wkt.WriteString("MULTIPOLYGON M(")
		geoString, byteCount = multi(wkbPolygon_Z_M, wkb)
	case 2007:
		wkt.WriteString("GEOMETRYCOLLECTION M(")
		geoString, byteCount = wkbGeometryCollection_Z_M(wkb)
	case 2015:
		wkt.WriteString("POLYHEDRALSURFACE M(")
		geoString, byteCount = wkbPolyhedralSurfaceTIN_Z_M(wkb)
	case 2016:
		wkt.WriteString("TIN M(")
		geoString, byteCount = wkbPolyhedralSurfaceTIN_Z_M(wkb)
	case 3001:
		wkt.WriteString("POINT ZM(")
		geoString, byteCount = wkbPoint_ZM(wkb)
	case 3002:
		wkt.WriteString("LINESTRING ZM(")
		geoString, byteCount = wkbLineString_ZM(wkb)
	case 3003:
		wkt.WriteString("POLYGON ZM(")
		geoString, byteCount = wkbPolygon_ZM(wkb)
	case 3004:
		wkt.WriteString("MULTIPOINT ZM(")
		geoString, byteCount = multi(wkbPoint_ZM, wkb)
	case 3005:
		wkt.WriteString("MULTILINESTRING ZM(")
		geoString, byteCount = multi(wkbLineString_ZM, wkb)
	case 3006:
		wkt.WriteString("MULTIPOLYGON ZM(")
		geoString, byteCount = multi(wkbPolygon_ZM, wkb)
	case 3007:
		wkt.WriteString("GEOMETRYCOLLECTION ZM(")
		geoString, byteCount = wkbGeometryCollection_ZM(wkb)
	case 3015:
		wkt.WriteString("POLYHEDRALSURFACE ZM(")
		geoString, byteCount = wkbPolyhedralSurfaceTIN_ZM(wkb)
	case 3016:
		wkt.WriteString("TIN ZM(")
		geoString, byteCount = wkbPolyhedralSurfaceTIN_ZM(wkb)
	}

	wkt.WriteString(geoString + ")")

	return wkt.String(), byteCount
}

// DoubleFromBinary takes an array of eight bytes and creates a double float number.
func doubleFromBinary(byteArray []byte, endian uint8) float64 {
	var convertUint64 uint64
	if endian == 1 {
		convertUint64 = binary.LittleEndian.Uint64(byteArray)
	} else {
		convertUint64 = binary.BigEndian.Uint64(byteArray)
	}
	return math.Float64frombits(convertUint64)
}

// get32Number processes an array of bytes and returns an uint32 number.
func get32Number(array []byte, endian uint8) uint32 {
	var num uint32
	if endian == 1 {
		num = binary.LittleEndian.Uint32(array)
	} else {
		num = binary.BigEndian.Uint32(array)
	}
	return num
}

// geoFunction is a type for passing functions on Multi.
type geoFunction func([]byte) (string, int)

// Multi takes a geometry function and calls it the number of times
// as perscribed in the binary.
func multi(fn geoFunction, wkb []byte) (string, int) {
	wkt := strings.Builder{}
	var byteCount int
	var i uint32

	childCount := get32Number(wkb[5:9], wkb[0])
	byteCount += 9

	for i = 0; i < childCount; i++ {
		geoString, bCount := fn(wkb[byteCount:])
		byteCount += bCount
		wkt.WriteString("(" + geoString + ")")
		if i < childCount-1 {
			wkt.WriteString(", ")
		}
	}

	return wkt.String(), byteCount
}
