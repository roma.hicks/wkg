// Copyright 2020 Roma Hicks

// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

package wellknowngeometry

var codeToGeoType map[uint32]string // Reverse map populated during initialisation.

var geoTypeToCode map[string]uint32 = map[string]uint32{
	"GEOMETRY":           0,
	"POINT":              1,
	"LINESTRING":         2,
	"POLYGON":            3,
	"MULTIPOINT":         4,
	"MULTILINESTRING":    5,
	"MULTIPOLYGON":       6,
	"GEOMETRYCOLLECTION": 7,
	"CIRCULARSTRING":     8,
	"COMPOUNDCURVE":      9,
	"CURVEPOLYGON":       10,
	"MULTICURVE":         11,
	"MULTISURFACE":       12,
	"CURVE":              13,
	"SURFACE":            14,
	"POLYHEDRALSURFACE":  15,
	"TIN":                16,

	"GEOMETRYZ":           1000,
	"POINTZ":              1001,
	"LINESTRINGZ":         1002,
	"POLYGONZ":            1003,
	"MULTIPOINTZ":         1004,
	"MULTILINESTRINGZ":    1005,
	"MULTIPOLYGONZ":       1006,
	"GEOMETRYCOLLECTIONZ": 1007,
	"CIRCULARSTRINGZ":     1008,
	"COMPOUNDCURVEZ":      1009,
	"CURVEPOLYGONZ":       1010,
	"MULTICURVEZ":         1011,
	"MULTISURFACEZ":       1012,
	"CURVEZ":              1013,
	"SURFACEZ":            1014,
	"POLYHEDRALSURFACEZ":  1015,
	"TINZ":                1016,

	"GEOMETRYM":           2000,
	"POINTM":              2001,
	"LINESTRINGM":         2002,
	"POLYGONM":            2003,
	"MULTIPOINTM":         2004,
	"MULTILINESTRINGM":    2005,
	"MULTIPOLYGONM":       2006,
	"GEOMETRYCOLLECTIONM": 2007,
	"CIRCULARSTRINGM":     2008,
	"COMPOUNDCURVEM":      2009,
	"CURVEPOLYGONM":       2010,
	"MULTICURVEM":         2011,
	"MULTISURFACEM":       2012,
	"CURVEM":              2013,
	"SURFACEM":            2014,
	"POLYHEDRALSURFACEM":  2015,
	"TINM":                2016,

	"GEOMETRYZM":           3000,
	"POINTZM":              3001,
	"LINESTRINGZM":         3002,
	"POLYGONZM":            3003,
	"MULTIPOINTZM":         3004,
	"MULTILINESTRINGZM":    3005,
	"MULTIPOLYGONZM":       3006,
	"GEOMETRYCOLLECTIONZM": 3007,
	"CIRCULARSTRINGZM":     3008,
	"COMPOUNDCURVEZM":      3009,
	"CURVEPOLYGONZM":       3010,
	"MULTICURVEZM":         3011,
	"MULTISURFACEZM":       3012,
	"CURVEZM":              3013,
	"SURFACEZM":            3014,
	"POLYHEDRALSURFACEZM":  3015,
	"TINZM":                3016,
}

// Init does some setup of the package. Specifically, prepares
// the reverse WKT and WKB map.
func Init() {
	codeToGeoType = make(map[uint32]string)
	for k, v := range geoTypeToCode {
		codeToGeoType[v] = k
	}
}

// SwapByteOrder changes byte order of a byte array/slice from big to little endian or vice-versa.
func SwapByteOrder(data []byte) []byte {
	swappedData := make([]byte, len(data))
	if len(data) == 0 {
		return swappedData
	}
	for i := range swappedData {
		swappedData[len(data)-1-i] = data[i]
	}
	return swappedData
}
